# Generated by Django 2.1.4 on 2018-12-10 17:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('formularz', '0006_auto_20181210_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='optymalizacja',
            name='lacza_sterowcow',
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name='optymalizacja',
            name='liczba_domkow',
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name='optymalizacja',
            name='zapotrzebowania_domkow',
            field=models.CharField(max_length=300),
        ),
    ]
