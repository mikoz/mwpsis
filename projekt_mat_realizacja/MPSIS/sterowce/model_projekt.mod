

param sterowce_l, integer, >= 0;	#liczba sterowców
param domki_l, integer, >=0;	#liczba domków
param d, integer, >= 0;	#zapotrzebowania

set Domki, default {1..domki_l};
set Sterowce, default {domki_l+1..domki_l+sterowce_l};# 1 - stacja bazowa, 2-domki_l - domki, domki_l+1-sterowce_l - sterowce
set Wezly default {1..(domki_l+sterowce_l)};
set D, default {1..d};

param M >=10000, default 99999;
param c, integer > 0, default 50; #max przepustowość danego sterowca

param luki{wezel1 in Wezly, wezel2 in Wezly}, binary, default 0;

param h{dd in D}, integer, >= 0;
param t{dd in D}, integer, >= 0;

var x{wezel1 in Wezly, wezel2 in Wezly, dd in D} integer, >= 0;

var v{wezel in Wezly}, binary; #wektor, które sterowce uruchamiamy


/* Objective function 'z' */
minimize z: sum{sterowiec in Sterowce} v[sterowiec];

/* ograniczenia NL */
s.t. c1{dd in D, sterowiec1 in Sterowce} : sum{sterowiec2 in Sterowce : luki[sterowiec2, sterowiec1] == 1} x[sterowiec2, sterowiec1, dd] + x[1,sterowiec1,dd] == sum{sterowiec2 in Sterowce : luki[sterowiec1, sterowiec2] == 1} x[sterowiec1, sterowiec2, dd] + sum{domek in Domki: luki[sterowiec1,domek]==1} x[sterowiec1,domek,dd];
s.t. c2{dd in D} : sum{sterowiec in Sterowce : luki[1, sterowiec] == 1} x[1, sterowiec, dd] == h[dd];
s.t. c3{dd in D} : sum{domek in Domki, sterowiec in Sterowce : luki[sterowiec, domek] == 1 and t[dd] == domek} x[sterowiec, domek, dd] == h[dd];
s.t. c4{sterowiec1 in Sterowce, sterowiec2 in Sterowce : luki[sterowiec1, sterowiec2] == 1} : sum{dd in D} x[sterowiec1, sterowiec2, dd] <= c;
s.t. c5{sterowiec in Sterowce, domek in Domki : luki[sterowiec, domek] == 1 and domek >=2 } : sum{dd in D} x[sterowiec, domek, dd] <= c;
s.t. c6{sterowiec1 in Sterowce} : sum{dd in D, sterowiec2 in Sterowce : luki[sterowiec1, sterowiec2] == 1} x[sterowiec1, sterowiec2, dd] + sum{dd in D, domek in Domki : luki[sterowiec1, domek] == 1 and domek >= 2 and t[dd] == domek} x[sterowiec1,domek,dd] <= M * v[sterowiec1];
s.t. c7{dd in D, wezel1 in Wezly, wezel2 in Wezly : luki[wezel1, wezel2] == 0} : x[wezel1, wezel2, dd] == 0; 

solve;

end;
