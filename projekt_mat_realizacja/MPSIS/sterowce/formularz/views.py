from django.shortcuts import render
from django.template.loader import render_to_string
from .forms import OptForm
from .models import Optymalizacja
import os.path

import hashlib
import time
import yaml, ast
import os
import re
import math

# Create your views here.

def index(request):
    return render(request, 'formularz/index.html', {})

def taskrequest(request):
    if request.method == "POST":
        form = OptForm(request.POST)
        if form.is_valid():
            opt_item = form.save(commit=False)
            opt_item.save()
            """with open("optymalizacje/" + str(opt_item.lokalizacjapliku), 'w') as f:
                _ = {
                    'nazwa': opt_item.nazwa,
                    'punkty': ast.literal_eval(opt_item.punkty),
                    'gestosc': opt_item.gestosc,
                    'zasieg': opt_item.zasieg,
                    'liczba_domkow': opt_item.liczba_domkow,
                    'zapotrzebowania_domkow': opt_item.zapotrzebowania_domkow,
                    'lacza_sterowcow': opt_item.lacza_sterowcow,
                }
                f.write(yaml.dump(_, default_flow_style=False))
                # [plik z danymi] [wspolrzedne obszaru w postaci listy] [liczba domkow] [zapotrzebowania domkow] [przepustowosc sterowcow] [zasieg] [gestosc]
                #os.system('python3 zeppelin.py %s' % opt_item.lokalizacjapliku)"""
                #return render(request, 'formularz/task_requested.html', {'form': form })
            arg_string = ""
            # nazwa pliku
            arg_string += opt_item.lokalizacjapliku[:-11] + " "

            # przepustosc
            arg_string += str(opt_item.lacza_sterowcow) + " "

            # zapotrzebowanie
            arg_string += str(opt_item.zapotrzebowania_domkow) + " "

            arg_string += str(len(ast.literal_eval(opt_item.punkty))) + " "
            for x, y in ast.literal_eval(opt_item.punkty):
                arg_string += "%s %s " % (x, y)
            arg_string += str(opt_item.zasieg) + " "
            arg_string += str(opt_item.gestosc) + " "
            os.system('./zeppelin ' + arg_string + ' &')
            return render(request, 'formularz/task_requested.html', {'form': form })
        else:
            return render(request, 'formularz/task_request.html', {'form': form })
    else:
        form = OptForm
        hash = hashlib.sha1()
        hash.update(str(time.time()).encode('utf-8'))
        nazwapliku = str(hash.hexdigest())
        return render(request, 'formularz/task_request.html', {'form': form, 'nazwapliku': nazwapliku})

def tasklist(request):
    lista = Optymalizacja.objects.all()
    for item in lista:
        item.czyzrobione = os.path.isfile("symulacje/" + str(item.lokalizacjapliku[:-11]) + ".result_raw")
        item.save()
    return render(request, 'formularz/task_list.html', {'list': lista})

def viewtask(request, id):
    lista = Optymalizacja.objects.filter(id=id)
    for item in lista:
        item.czyzrobione = os.path.isfile("symulacje/" + str(item.lokalizacjapliku[:-11]) + ".result_raw")
        item.save()
    return render(request, 'formularz/task_detail.html', {'list': lista})

def detailtask(request, id):
    lista = Optymalizacja.objects.filter(id=id)
   
    pozycje = []
    nazwa = ""
    idv = ""
    obszar = []
    for item in lista:
        str_1 = "symulacje/" + str(item.lokalizacjapliku[:-11]) + "_pozycje.txt"
        str_2 = "symulacje/" + str(item.lokalizacjapliku[:-11]) + ".result_raw"
        nazwa = item.nazwa
        obszar.append(ast.literal_eval(item.punkty))
        idv = item.id
        with open("%s" % str_1 , "r") as f:
            for line in f:
                pozycje.append(line.split())
    
    with open("%s" % str_2, "r") as f:
        sterowce = re.findall(r'\sv\[(\d+)\]', f.read())

    sterowce_indeksy = [ int(ster) for ster in sterowce ]
    print(sterowce_indeksy)

    sterowce = []
    domki = []
    stacje = []
    ilosc_stacji_i_domkow = 0
    for item in pozycje:
        if item[0] == "stacja":
            stacje.append([item[2], item[3]])
        if item[0] == "domek":
            domki.append([item[2], item[3]])

    for item in sterowce_indeksy:
        sterowce.append([pozycje[item-1][2], pozycje[item-1][3]])

    return render(request, 'formularz/view.html', {'nazwa': nazwa, 'sterowce': sterowce, 
        'domki': domki,
        'stacje': stacje,
        'id': idv,
        'obszar': obszar[0]
        })
