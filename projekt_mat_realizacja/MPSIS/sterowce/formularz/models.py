from django.db import models

# Create your models here.

class Optymalizacja(models.Model):
    class Meta:
        verbose_name_plural = "Optymalizacje"

    nazwa = models.CharField(max_length=300)
    punkty = models.CharField(max_length=300)
    gestosc = models.FloatField(default=1.0)
    zasieg = models.FloatField(default=1.1)
    lokalizacjapliku = models.CharField(max_length=300)
    liczba_domkow = models.PositiveSmallIntegerField(default=5)
    zapotrzebowania_domkow = models.CharField(max_length=5)
    lacza_sterowcow = models.CharField(max_length=5)
    czyzrobione = models.BooleanField(default=False)


    def __str__(self):
        return self.nazwa