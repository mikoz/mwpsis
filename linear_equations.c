#include <stdio.h>
#include <stdlib.h>

struct Punkt{
	//struktura przechowująca współrzędne dowolnego punktu
	float x;
	float y;
};

struct FunkcjaOgraniczajacaPrzestrzen{
	/*struktura przechowująca parametry funkcji 'y <= ax + b' lub 'y >= ax + b'
	  na ten moment dla uproszczenia zakładamy, ze znak może przyjmować 
	  wartość '<' lub '>', ale traktujemy je jako '<=' i '>='
	*/
	float a;
	float b;
	char znak;
};


struct FunkcjaOgraniczajacaPrzestrzen ZwrocFunkcje(struct Punkt A, struct Punkt B){
	/*wyznacza wzór funkcji liniowej przechodzącej przej punkty A i B oraz określa
	czy dana funkcja ogranicza obszar od góry czy od dołu - przyjmujemy założenie, że użytkownik podaje punkty tak, aby kolejno wyznaczały krawędzie
	wypukłego obszaru
	*/
	struct FunkcjaOgraniczajacaPrzestrzen f;
	
	f.a = (A.y-B.y)/(A.x-B.x);
	f.b = A.y - (A.y-B.y)*A.x/(A.x-B.x);

	if(B.x > A.x) f.znak = '<';
	else f.znak = '>';

	return f;
}

int CzyPunktWObszarze(struct FunkcjaOgraniczajacaPrzestrzen tab_f[], struct Punkt P){
	//sprawdza czy przekazany do funkcji punkt znajduje się w obszarze wyznaczonym przez funkcje z przekazanej tabeli czy nie
	int i;

	for(i = 0; i < sizeof(tab_f); i++){
		if(tab_f[i].znak == '<' && P.y > tab_f[i].a * P.x + tab_f[i].b)
			return 0;
		else if(tab_f[i].znak == '>' && P.y < tab_f[i].a * P.x + tab_f[i].b)
			return 0;
	}

	return 1;
}


int main(){
	//póki co statycznie ustawiamy ilość punktów
	int s = 5;

	//utworzenie wskaźników na tablice punktów i funkcji
	struct Punkt *tab_p;
	struct FunkcjaOgraniczajacaPrzestrzen *tab_f;

	//punkt, dla którego sprawdzamy czy znajduje się w obszarze czy nie
	struct Punkt P;
	P.x = 2;
	P.y = 10;

	//alokacja pamięci na tablice
	tab_p = malloc(s * sizeof(struct Punkt));
	tab_f = malloc(s * sizeof(struct FunkcjaOgraniczajacaPrzestrzen));

	//podanie współrzędnych punktów wyznaczających obszar
	tab_p[0].x = 0;
	tab_p[0].y = 2;
	tab_p[1].x = 1;
	tab_p[1].y = 4;
	tab_p[2].x = 5;
	tab_p[2].y = 5;
	tab_p[3].x = 4;
	tab_p[3].y = 2;
	tab_p[4].x = 2;
	tab_p[4].y = 1;

	int i;

	//wypełnienie tablicy tab_f funkcjami ograniczającymi obszar
	for(i = 0; i < s; i++){
		if(i == s - 1) tab_f[i] = ZwrocFunkcje(tab_p[s - 1], tab_p[0]);
		else tab_f[i] = ZwrocFunkcje(tab_p[i], tab_p[i+1]);
	}


	//wypisanie wzorów funkcji -> można wkleić w wolfram i widać obszar
	for(i = 0; i < s; i++){
		printf("y %c %0.3fx+(%0.3f) & ", tab_f[i].znak, tab_f[i].a, tab_f[i].b);
	}


	//sprawdzenie czy punkt należy do obszaru
	if(CzyPunktWObszarze(tab_f, P) == 1)
		printf("\n\n TAK \n\n");
	else
		printf("\n\n NIE \n\n");
	return 0;

}