#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <string>
#include <array>
#include <math.h>
#include <vector>
#include <cstdio>
#include <cmath>
#include <fstream>
#include <sys/stat.h>

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"



using namespace std;
using namespace cgicc;

const int ARRAY_SIZE = 1000;

static const string alphanum = "0123456789abcdefghijklmnopqrstuvwxyz";

unsigned long long rdtsc(){
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((unsigned long long)hi << 32) | lo;
}

class Punkt {
    public:
        float x,y ;
        Punkt() {}
        Punkt(float new_x, float new_y){
            x = new_x;
            y = new_y;
        }
};


class FunkcjaOgraniczajaca {
    public: 
        float a, b;
        string znak;

        FunkcjaOgraniczajaca(Punkt pkt_a, Punkt pkt_b) {
            a = ((pkt_a.y - pkt_b.y) / (pkt_a.x - pkt_b.x));
            b = (pkt_a.y - ((pkt_a.y - pkt_b.y) * pkt_a.x / (pkt_a.x - pkt_b.x)));
            
            if(pkt_b.x > pkt_a.x) {
                znak = '<';
            } else {
                znak = '>';
            }

        }
};


class Plansza {
    public:
        vector<Punkt> punkty;
        float zasieg;
        float gestosc;
        float min_x, min_y, max_x, max_y;
        vector<FunkcjaOgraniczajaca> funkcje_ograniczajace;
        Punkt stacja;
        vector<Punkt> pozycje_sterowcow;
        vector<Punkt> pozycje_domkow;     
        string wynik;
        string nazwa_symulacji = "";
        typedef vector<vector <int>> Matrix;
        typedef vector<int> Row;
        Matrix tablica_sasiedztw;  
        string przepustowosc;   
        string zapotrzebowanie;   


        Plansza(string nazwa, string nowa_przepustowosc, string nowa_zapotrzebowanie, vector<Punkt> nowe_punkty, float nowy_zasieg, float nowa_gestosc) {

            zasieg = nowy_zasieg;
            gestosc = nowa_gestosc;
            punkty = nowe_punkty;
            min_x = punkty[0].x, min_y = punkty[0].y, max_x = punkty[0].x, max_y = punkty[0].y;
            //cout << "elo" <<endl;
            //generuj_nazwe_symulacji();
            nazwa_symulacji = string(nazwa);
            przepustowosc = string(nowa_przepustowosc);
            zapotrzebowanie = string(nowa_zapotrzebowanie);
            //cout << "elo2" << endl;
            generuj_max();
            //cout << "2" << endl;
            generuj_min();
            //cout << "3" << endl;
            
            generuj_funkcje_ograniczajace();
            
            generuj_pozycje_sterowcow();
            //cout << "5" << endl;            
            generuj_pozycje_stacji();
            //cout << "6" << endl;
            generuj_pozycje_domkow();
            //cout << "7" << endl;
            generuj_tablice_sasiedztw();
            //cout << "8" << endl;
            zapisz_pozycje();
            zapisz_sasiedztwa();
            uruchom_symulacje();
        }

    private:
        void generuj_nazwe_symulacji(){
            for(unsigned int i = 0; i < 16; i++){
                nazwa_symulacji = nazwa_symulacji + alphanum[rand() % (sizeof(alphanum) - 1)];
            }

        }

        void generuj_funkcje_ograniczajace() {
            for (int i=0; i < punkty.size(); i++){
                if(punkty.size() -1 == i){
                    funkcje_ograniczajace.push_back(FunkcjaOgraniczajaca(punkty[i], punkty[0]));
                } else {
                    funkcje_ograniczajace.push_back(FunkcjaOgraniczajaca(punkty[i], punkty[i+1]));
                }
            }
            for (int i=0; i < punkty.size(); i++){
                //cout << "Funkcje ograniczajaca: y " << funkcje_ograniczajace[i].znak << funkcje_ograniczajace[i].a << "*x + " << funkcje_ograniczajace[i].b << endl;
            }
        }

        void generuj_min() {
            for (int i=0; i < punkty.size(); i++){
                if(punkty[i].x < min_x){
                    min_x = punkty[i].x;
                }
                if(punkty[i].y < min_y){
                    min_y = punkty[i].y;
                }
            }
        }

        void generuj_max() {
            for (int i=0; i < punkty.size(); i++){
                if(punkty[i].x > max_x){
                    max_x = punkty[i].x;
                }
                if(punkty[i].y > max_y){
                    max_y = punkty[i].y;
                }
            }
        }

        void generuj_pozycje_stacji() {
            stacja = Punkt((max_x - min_x)/2 + min_x, (max_y - min_y)/2 + min_y);

            while(punkt_w_obszarze(stacja) == true){
                stacja = Punkt(round(((float) rand()/(RAND_MAX) * (max_x - min_x) + min_x) * 1000.0) / 1000.0,
                                round(((float) rand()/(RAND_MAX) * (max_x - min_x) + min_x) * 1000.0) / 1000.0);
            }
        }


        void generuj_pozycje_domkow() {
            Punkt domek;

            for(int i=0; i <= 20; i++){
                domek = Punkt(round(((float) rand()/(RAND_MAX) * (max_x - min_x) + min_x) * 1000.0) / 1000.0,
                                round(((float) rand()/(RAND_MAX) * (max_y - min_y) + min_y) * 1000.0) / 1000.0);
                //cout << "domek" << i <<endl;
                while(!punkt_w_obszarze(domek)){
                    domek = Punkt(round(((float) rand()/(RAND_MAX) * (max_x - min_x) + min_x) * 1000.0) / 1000.0,
                                    round(((float) rand()/(RAND_MAX) * (max_y - min_y) + min_y) * 1000.0) / 1000.0);
                }
                pozycje_domkow.push_back(domek);
                //cout << domek.x << " " << domek.y << " " << punkt_w_obszarze(domek) << endl;
            }
        }

        void generuj_pozycje_sterowcow() {
            float temp_y = min_y;
            float temp_x = min_x;
            //int i = 0;
            
            while(temp_y <= max_y) {
                while(temp_x <= max_x) {
                    pozycje_sterowcow.push_back(Punkt(round(temp_x * 1000.0)/1000.0, round(temp_y * 1000.0)/1000.0));
                    temp_x += 1 / gestosc;
                    //i += 1;
                }
            temp_x = min_x;
            temp_y += 1 / gestosc;
            }
        }

        bool punkt_w_obszarze(Punkt pkt_a) {
            for (int i=0; i < funkcje_ograniczajace.size(); i++){
                //cout << "size: " << funkcje_ograniczajace.size() << endl; 
                //czemu było i=1 ??
                if(funkcje_ograniczajace[i].znak == "<" && (pkt_a.y > funkcje_ograniczajace[i].a * pkt_a.x + funkcje_ograniczajace[i].b)){
                    
                    //cout << " tu wpadam " << i << " " << pkt_a.y << " " << funkcje_ograniczajace[i].a * pkt_a.x + funkcje_ograniczajace[i].b << endl;

                    return false;
                } else if(funkcje_ograniczajace[i].znak == ">" && (pkt_a.y < funkcje_ograniczajace[i].a * pkt_a.x + funkcje_ograniczajace[i].b) ) {
                    //cout << "a nie bo tu" << i << " " << pkt_a.y << " " << funkcje_ograniczajace[i].a * pkt_a.x + funkcje_ograniczajace[i].b << endl;
                    return false;
                }
            }
            return true;
        }

        void generuj_tablice_sasiedztw(){
            vector<Punkt> wszystkie_obiekty;
            wszystkie_obiekty.push_back(stacja);

            wszystkie_obiekty.insert(wszystkie_obiekty.end(), pozycje_domkow.begin(), pozycje_domkow.end());
            wszystkie_obiekty.insert(wszystkie_obiekty.end(), pozycje_sterowcow.begin(), pozycje_sterowcow.end());

            int size = wszystkie_obiekty.size();
            float nowa_odleglosc; 

            for(int i=0; i < wszystkie_obiekty.size(); ++i){
                Row row(size);
                for(int j; j < wszystkie_obiekty.size(); ++j){
                    row[j] = 0;
                }

                tablica_sasiedztw.push_back(row);
            }

            for(int i=0; i < wszystkie_obiekty.size(); i++){
                for(int j=0; j < wszystkie_obiekty.size(); j++){
                    nowa_odleglosc = sqrt(pow(wszystkie_obiekty[j].x - wszystkie_obiekty[i].x, 2) + 
                    (pow(wszystkie_obiekty[j].y - wszystkie_obiekty[i].y, 2)));

                    if(nowa_odleglosc < zasieg){
                        tablica_sasiedztw[i][j] = 1;
                    } else {
                        tablica_sasiedztw[i][j] = 0;
                    }
                }
                tablica_sasiedztw[i][i] = 0;
            }

        }


        void zapisz_pozycje(){
            ofstream f;
            string sciezka = "./symulacje/" + nazwa_symulacji  + "_pozycje.txt";
            f.open(sciezka);

            f << "stacja 1 " << stacja.x << " " << stacja.y << endl;
            
            for(int i=0; i < pozycje_domkow.size(); i++){
                f << "domek " << i << " " << pozycje_domkow[i].x << " " << pozycje_domkow[i].y << endl;
            }

            for(int i=0; i < pozycje_sterowcow.size(); i++){
                f << "sterowiec " << i << " " << pozycje_sterowcow[i].x << " " << pozycje_sterowcow[i].y << endl;
            }

            f.close();

        }

        void zapisz_sasiedztwa(){

            ofstream f;
            string sciezka = "./symulacje/" + nazwa_symulacji  + ".dat";
            f.open(sciezka);

            wynik = "data;\n";
            wynik += "param sterowce_l := " + to_string(pozycje_sterowcow.size());
            wynik += ";\nparam domki_l := " + to_string(pozycje_domkow.size() + 1);
            wynik += ";\nparam d := " + to_string(pozycje_domkow.size());
            wynik += ";\nparam : luki :=\n";

            for(int i=0; i < (int) tablica_sasiedztw.size(); i++){

                for(int j=1; j < tablica_sasiedztw[i].size(); j++){
                    
                    if (tablica_sasiedztw[i][j] == 1){
                        //cout << "elo";
                        // to_string(i+1);
                        wynik +=  to_string(i+1) + " " + to_string(j+1) + " " + to_string(tablica_sasiedztw[i][j]) + "\n";
                    }
                }
            }


            wynik += ";\nparam : h :=\n";

            for(int i=0; i < pozycje_domkow.size(); i++){
                wynik += to_string(i+1) + " " + zapotrzebowanie + "\n";
            }
            
            wynik += ";\nparam : t :=\n";
            

            for(int i=0; i < pozycje_domkow.size(); i++){
                wynik += to_string(i + 1) + " " + to_string(i + 2) + "\n";
            }

            wynik += ";\n";
            wynik += "param c := " + przepustowosc + " ;\n";
            wynik += "end;\n";

            f << wynik;
            f.close();

        }


        void uruchom_symulacje(){
            //string cbc_string = "model_projekt.mod\%symulacje/" + nazwa_symulacji + ".dat " + "solve solu " + "symulacje/" + nazwa_symulacji + ".result_raw &> /dev/null";
            string cbc_string = "cbc model_projekt.mod\%symulacje/" + nazwa_symulacji + ".dat " + "solve solu " + "symulacje/" + nazwa_symulacji + ".result_raw";
            system(cbc_string.c_str());
        }



};



int main(int argc, char *argv[]) {

    /*srand(rdtsc());

    int s = 5;

    vector<Punkt> punkty;
    //Punkt pkt_a = Punkt(2,4);

    punkty.push_back(Punkt(1,1));
    punkty.push_back(Punkt(2,6));
    punkty.push_back(Punkt(9,5));
    punkty.push_back(Punkt(5,1));


    Plansza p = Plansza(punkty, 2.1, 0.5);*/

    /*
        ./program [nazwa] [przepustowosc] [zapotrzebowanie] [liczba punktow obszaru] { 2 argumenty per punkt } [zasieg sterowca] [ilosc sterowcow na jednostke]
            0        1           2             3                     4                  5 ..
    */

    Cgicc formData;
    vector<Punkt> vpunkty;

    /*   // Send HTTP header: Content-type: text/html
       cout << HTTPHTMLHeader() << endl;

       // Print: <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
       cout << HTMLDoctype(HTMLDoctype::eStrict) << endl;

       // Print: <html lang="en" dir="LTR">
       cout << html().set("lang", "EN").set("dir", "LTR") << endl;

       // Set up the HTML document
       cout << html() << head() << title("Cgicc example") << head() << endl;
       cout << body().set("bgcolor","#cccccc").set("text","#000000").set("link","#0000ff").set("vlink","#000080") << endl;

       cout << h1("This is a demonstration of the GNU CgiCC library") << endl;

       form_iterator fvalue1 = formData.getElement("value1");
       if( !fvalue1->isEmpty() && fvalue1 != (*formData).end()) 
*/
cout << HTTPHTMLHeader() << endl;

       // Print: <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
       cout << HTMLDoctype(HTMLDoctype::eStrict) << endl;

       // Print: <html lang="en" dir="LTR">
       cout << html().set("lang", "EN").set("dir", "LTR") << endl;

       // Set up the HTML document
       cout << html() << head() << title("Cgicc example") << meta().set( "HTTP-EQUIV", "Content-type" ).set( "CONTENT", "text/html; charset=utf-8" ) << head() << endl;
       cout << body().set("bgcolor","#cccccc").set("text","#000000").set("link","#0000ff").set("vlink","#000080") << endl;

       cout << h1("Wynik symulacji") << endl;

    form_iterator nazwa = formData.getElement("nazwa");
       if( !nazwa->isEmpty() && nazwa != (*formData).end()) {
          cout << "Nazwa symulacji: " << **nazwa << endl;
    }
        cout << p();
        form_iterator gestosc = formData.getElement("gestosc");
       if( !gestosc->isEmpty() && gestosc != (*formData).end()) {
          cout << "Gęstość siatki: " << **gestosc << endl;
    }
        cout << p();
        form_iterator zasieg = formData.getElement("zasieg");
       if( !zasieg->isEmpty() && zasieg != (*formData).end()) {
          cout << "Zasięg sterowców: " << **zasieg << endl;
    }
    cout << p();
        form_iterator zapotrzebowania_domkow = formData.getElement("zapotrzebowania_domkow");
       if( !zapotrzebowania_domkow->isEmpty() && zapotrzebowania_domkow != (*formData).end()) {
          cout << "Zapotrzebowania domków: " << **zapotrzebowania_domkow << endl;
    }
    cout << p();
        form_iterator lacza_sterowcow = formData.getElement("lacza_sterowcow");
       if( !lacza_sterowcow->isEmpty() && lacza_sterowcow != (*formData).end()) {
          cout << "Przepustowość łącz sterowców: " << **lacza_sterowcow << endl;
    }
    cout << p() << "Punkty: " << p();
       // Close the HTML document

    for(int i = 0; i < (formData.getElements().size() - 5)/2; i++){
    	
    	if(i == 0){
    		form_iterator temp_x = formData.getElement("x");
    		form_iterator temp_y = formData.getElement("y");
    		
    		vpunkty.push_back(Punkt(stoi(**temp_x),stoi(**temp_y)));
            cout << "(" << **temp_x << "," << **temp_y << ") "; 
    	}
    	else{
			form_iterator temp_x = formData.getElement("x" + to_string(i));
    		form_iterator temp_y = formData.getElement("y" + to_string(i));
    		
    		vpunkty.push_back(Punkt(stoi(**temp_x),stoi(**temp_y)));
            cout << "(" << **temp_x << "," << **temp_y << ") ";	
    	}
	}

       
    cout << p() << p() << p();


    srand(rdtsc());
    
    pid_t pid = fork();
    if (pid == 0)
    {
        cout << body() << html();
        return 0;
        
    }
    else if (pid > 0)
    {
        //close(1);
        Plansza p = Plansza(**nazwa, **lacza_sterowcow, **zapotrzebowania_domkow, vpunkty, stof(**zasieg), stof(**gestosc));
        string result = "cat symulacje/" + **nazwa + ".result_raw";
        system(result.c_str());
        int status;
        while( wait(&status) > 0) { /* no-op */ ; }
    }
    else
    {
        // fork failed
        printf("fork() failed!\n");
        return 1;
    }
    
           
 
    return 0;
}













