
from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('request', views.taskrequest, name='taskrequest'),
    path('request/', views.taskrequest, name='taskrequest'),
    path('list', views.tasklist, name='tasklist'),
    path('list/', views.tasklist, name='tasklist'),
    re_path('^list/(?P<id>\d+)/$', views.viewtask, name='viewtask'),
    re_path('^view/(?P<id>\d+)/$', views.detailtask, name='detailtask'),
    #path('list', views.tasklist, name='tasklist'),


] 
