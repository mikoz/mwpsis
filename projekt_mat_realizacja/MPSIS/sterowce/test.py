import re

def parse_output(filename):
	regex = r"\sv\[(\d+)\]\s+1\s+1"
	p = re.compile(regex)
	v = []
	with open(filename, 'r') as f:
		for line in f:
			_ = p.search(line)
			if _:
				v.append(_.group(1))
	return v

def make_output(sasiedztwa):
	_ = "param : luki :=\n"
	for i in range(0,len(sasiedztwa)):
		for j in range(0,len(sasiedztwa[i])):
			if sasiedztwa[i][j]:
				_ += "%s,%s %s\n" % (i,j,sasiedztwa[i][j]) 
	_ += ";\n"
	return _

def main():
	#print(parse_output('optymalizacje/2b913008eb6e23a5e373e733eeb212ed312d8bb9.opt.out'))
	t = [[0,0,1,0], [1,0,0,0], [0,0,0,1], [1,1,0,0]]
	print(make_output(t))

if __name__ == "__main__":
	main()
