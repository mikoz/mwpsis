#!/usr/bin/python3

import random
from math import sqrt, pow

class Punkt:
	
	def __init__(self, new_x, new_y):
		
		self.x = new_x
		self.y = new_y

class FunkcjaOgraniczajaca:
	
	def __init__(self, pkt_a, pkt_b):
		
		self.a = (pkt_a.y-pkt_b.y)/(pkt_a.x-pkt_b.x)
		self.b = pkt_a.y - (pkt_a.y-pkt_b.y)*pkt_a.x/(pkt_a.x-pkt_b.x)
		if(pkt_b.x > pkt_a.x):
			self.znak = '<'
		else:
			self.znak = '>'


class Plansza:

	def __init__(self, new_punkty, new_zasieg, new_gestosc):

		self.punkty = new_punkty
		self.zasieg = new_zasieg
		self.gestosc = new_gestosc
		self.funkcje_ograniczajace = self.generuj_funkcje_ograniczajace()

		self.stacja = self.generuj_pozycje_stacji()
		self.pozycje_sterowcow = self.generuj_pozycje_sterowcow()
		self.pozycje_domkow = self.generuj_pozycje_domkow()

		self.tablica_sasiedztw = self.generuj_tablice_sasiedztw()

	def generuj_funkcje_ograniczajace(self):
		
		funkcje = []

		for i in range(len(self.punkty)):
			if i == len(self.punkty) - 1:
				funkcje.append(FunkcjaOgraniczajaca(self.punkty[i], self.punkty[0]))
			else:
				funkcje.append(FunkcjaOgraniczajaca(self.punkty[i], self.punkty[i+1]))

		return funkcje

	def generuj_pozycje_stacji(self):

		min_x = min([pkt_a.x for pkt_a in self.punkty])
		min_y = min([pkt_a.y for pkt_a in self.punkty])
		max_x = max([pkt_a.x for pkt_a in self.punkty])
		max_y = max([pkt_a.y for pkt_a in self.punkty])

		stacja = Punkt((max_x - min_x)/2, (max_y - min_y)/2)

		while self.punkt_w_obszarze(stacja):
			stacja = Punkt(round(random.random()*(max_x - min_x) + min_x, 3),
			round(random.random()*(max_y - min_y) + min_y, 3))

		return stacja

	def generuj_pozycje_domkow(self):

		min_x = min([pkt_a.x for pkt_a in self.punkty])
		min_y = min([pkt_a.y for pkt_a in self.punkty])
		max_x = max([pkt_a.x for pkt_a in self.punkty])
		max_y = max([pkt_a.y for pkt_a in self.punkty])

		stacja = Punkt((max_x - min_x)/2, (max_y - min_y)/2)

		domki = []

		for i in range(int(len(self.pozycje_sterowcow)/25)):

			domek = Punkt(round(random.random()*(max_x - min_x) + min_x, 3),
			round(random.random()*(max_y - min_y) + min_y, 3))

			while not self.punkt_w_obszarze(domek):
				domek = Punkt(round(random.random()*(max_x - min_x) + min_x, 3),
				round(random.random()*(max_y - min_y) + min_y, 3))

			domki.append(domek)

		return domki

	def generuj_pozycje_sterowcow(self):

		sterowce = []

		min_x = min([pkt_a.x for pkt_a in self.punkty])
		min_y = min([pkt_a.y for pkt_a in self.punkty])
		max_x = max([pkt_a.x for pkt_a in self.punkty])
		max_y = max([pkt_a.y for pkt_a in self.punkty])

		temp_x = min_x - self.zasieg
		temp_y = min_y - self.zasieg

		while temp_y <= max_y + self.zasieg:
			while temp_x <= max_x + self.zasieg:
				sterowce.append(Punkt(round(temp_x, 3), round(temp_y, 3)))
				temp_x += 1/self.gestosc
			temp_x = min_x - self.zasieg
			temp_y += 1/self.gestosc

		return sterowce

	def punkt_w_obszarze(self, pkt_a):

		for funkcja in self.funkcje_ograniczajace:
			if funkcja.znak == "<" and pkt_a.y > funkcja.a * pkt_a.x + funkcja.b:
				return False
			elif funkcja.znak == ">" and pkt_a.y < funkcja.a * pkt_a.x + funkcja.b:
				return False
			else:
				pass
		return True

	def generuj_tablice_sasiedztw(self):

		wszystkie_obiekty = []
		wszystkie_obiekty.append(self.stacja)
		for domek in self.pozycje_domkow: wszystkie_obiekty.append(domek)
		for sterowiec in self.pozycje_sterowcow: wszystkie_obiekty.append(sterowiec)

		tablica_sasiedztw = [[0 for x in range(len(wszystkie_obiekty))] for y in range(len(wszystkie_obiekty))]

		for obiekt_id, obiekt in enumerate(wszystkie_obiekty):

			for potencjalny_sasiad_id, potencjalny_sasiad in enumerate(wszystkie_obiekty):
				odleglosc = sqrt(pow(potencjalny_sasiad.x - obiekt.x, 2) + (pow(potencjalny_sasiad.y - obiekt.y, 2)))
				if odleglosc < self.zasieg:
					tablica_sasiedztw[obiekt_id][potencjalny_sasiad_id] = 1
				else:
					tablica_sasiedztw[obiekt_id][potencjalny_sasiad_id] = 0

			tablica_sasiedztw[obiekt_id][obiekt_id] = 0

		return tablica_sasiedztw

	def zapisz_sasiedztwa(self):
		_ = "data;\n"
		_ += "param sterowce_l := %s;\n" % (len(self.pozycje_sterowcow))
		_ += "param domki_l := %s;\n" % (len(self.pozycje_domkow)+1)
		_ += "param d := %s;\n" % (len(self.pozycje_domkow))
		_ += "param : luki :=\n"
		for i in range(0,len(self.tablica_sasiedztw)):
			for j in range(1,len(self.tablica_sasiedztw[i])):
				if self.tablica_sasiedztw[i][j]:
					_ += "%s,%s %s\n" % (i+1,j+1,self.tablica_sasiedztw[i][j]) 
		_ += ";\n"
		_ += "param : h :=\n"
		for i in range(0,len(self.pozycje_domkow)):
			_ += "%s 75\n" % (i+1)
		_ += ";\n"
		_ += "param : t :=\n"
		for i in range(0,len(self.pozycje_domkow)):
			_ += "%s %s\n" % (i+1, i+2)
		_ += ";\n"
		_ += "end;\n"
		return _

def main():
	
	s = 5

	punkty = []
	funkcje = []

	pkt_a = Punkt(2,4)

	punkty.append(Punkt(0,2))
	punkty.append(Punkt(1,4))
	punkty.append(Punkt(5,5))
	punkty.append(Punkt(4,2))
	punkty.append(Punkt(2,1))

	p = Plansza(punkty, 1.1, 1)

	"""
	for funkcja in p.funkcje_ograniczajace:
		print("y {} {:.3f}x+({:.3f}) \n".format(funkcja.znak, funkcja.a, funkcja.b))

	for sterowiec in p.pozycje_sterowcow:
		print("Sterowiec {} ma koordynaty ({},{})".format([i for i,x in enumerate(p.pozycje_sterowcow) if x == sterowiec], sterowiec.x, sterowiec.y))

	for domek in p.pozycje_domkow:
		print("Domek {} ma koordynaty ({},{})".format([i for i,x in enumerate(p.pozycje_domkow) if x == domek], domek.x, domek.y))
	"""
	#print("Pozycja stacji to ({},{})".format(p.stacja.x, p.stacja.y))

	print(p.zapisz_sasiedztwa())




	#print(p.punkt_w_obszarze(p.stacja))

main()