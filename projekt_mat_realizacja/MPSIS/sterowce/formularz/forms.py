from django.forms import ModelForm
from .models import Optymalizacja

class OptForm(ModelForm):
	class Meta:
		model = Optymalizacja
		fields = ['nazwa', 
				  'punkty',
				  'gestosc',
				  'zasieg', 
				  'lokalizacjapliku', 
				  #'liczba_domkow', 
				  'zapotrzebowania_domkow', 
				  'lacza_sterowcow'
				  ]